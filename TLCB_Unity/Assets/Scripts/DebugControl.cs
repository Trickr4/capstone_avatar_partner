﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class DebugControl : MonoBehaviour
{
    [SerializeField] GameObject Tracking;

    void OnEnable()
    {
        Tracking.GetComponent<ARPlaneManager>().enabled=true;
        Debug.Log("true");
    }

    void OnDisable()
    {
        foreach(ARPlane plane in Tracking.GetComponent<ARPlaneManager>().trackables)
        {
            plane.gameObject.SetActive(!Tracking.GetComponent<ARPlaneManager>().enabled);
        }
        Tracking.GetComponent<ARPlaneManager>().enabled=false;
        Debug.Log("false");
    }
}
