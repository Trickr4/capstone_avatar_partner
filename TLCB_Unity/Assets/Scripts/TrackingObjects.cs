using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

[RequireComponent(typeof(ARTrackedImageManager))]
public class TrackingObjects : MonoBehaviour
{
    [SerializeField]
    private GameObject[] objectList;

    private ARTrackedImageManager trackedImageManager;

    private void Awake() 
    {
        trackedImageManager = FindObjectOfType<ARTrackedImageManager>();
    }

}
