﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

[RequireComponent(typeof(ARRaycastManager))]
public class PlaneObject : MonoBehaviour
{
    ARRaycastManager m_RaycastManager;
    [SerializeField]
    GameObject m_PlacedPrefab;

    public GameObject placedPrefab
        {
            get { return m_PlacedPrefab; }
            set { m_PlacedPrefab = value; }
        }

    public GameObject spawnedObject { get; private set; }

    void Awake()
        {
            m_RaycastManager = GetComponent<ARRaycastManager>();
        }

    bool TryGetTouchPosition(out Vector2 touchPosition)
        {
            if (Input.touchCount > 0)
            {
                touchPosition = Input.GetTouch(0).position;
                return true;
            }

            touchPosition = default;
            return false;
        }

        void Update()
        {
            if (!TryGetTouchPosition(out Vector2 touchPosition))
                return;

            if (m_RaycastManager.Raycast(touchPosition, s_Hits, TrackableType.PlaneWithinPolygon))
            {
                // Raycast hits are sorted by distance, so the first one
                // will be the closest hit.
                var hitPose = s_Hits[0].pose;
                m_PlacedPrefab.transform.position = hitPose.position;

            }
        }

        static List<ARRaycastHit> s_Hits = new List<ARRaycastHit>();
}
